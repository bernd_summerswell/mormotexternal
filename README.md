**Synopse mORMot Auto-Sync**

Read the Readme.txt for more information

Synopse mORMot framework

An Open Source Client-Server ORM/SOA framework
 (c) 2008-2014 Synopse Informatique
  http://synopse.info
  http://mormot.net
  
I have setup a auto-sync script that will pull down the nightly from http://synopse.info/files/mORMotNightlyBuild.zip and commit any changes to this Git Repo